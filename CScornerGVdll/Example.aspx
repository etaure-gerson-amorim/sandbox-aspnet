﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Example.aspx.cs" Inherits="CScornerGVdll.Example" %>

<%@ Register Assembly="SamApp.WebControls.SearchGridView" Namespace="SamApp.WebControls" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <div style="height: 100px">



            <asp:HiddenField ID="hfSearchText" runat="server" />

            <asp:HiddenField ID="hfSort" runat="server" />

        </div>

        <div>

            <cc1:SearchGridView ID="SearchGridView1" runat="server" BackColor="White"
                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                OnPageIndexChanging="SearchGridView1_PageIndexChanging"
                OnSearchGrid="SearchGridView1_SearchGrid" ShowFooter="True"
                AllowPaging="True" AllowSorting="True"
                OnSorting="SearchGridView1_Sorting" ForeColor="Black" GridLines="Vertical" DataSourceID="SqlDataSource1">
                
                <SearchFilters>
                    <%--<asp:ListItem>Username</asp:ListItem>
                    <asp:ListItem Value="User_Pass">Password</asp:ListItem>
                    <asp:ListItem Value="F_Name">First Name</asp:ListItem>
                    <asp:ListItem Value="L_Name">Last Name</asp:ListItem>
                    <asp:ListItem Value="City">City</asp:ListItem>--%>

                    <asp:ListItem Value="BankName">Nome do banco</asp:ListItem>
                    <asp:ListItem Value="BankId">ID Banco</asp:ListItem>
                    <asp:ListItem Value="BankBACENCode">BACEN</asp:ListItem>
                </SearchFilters>

                <AlternatingRowStyle BackColor="#CCCCCC" />

                <Columns>

                    <%--<asp:BoundField DataField="Username" HeaderText="Username"
                        SortExpression="Username" />
                    <asp:BoundField DataField="User_Pass" HeaderText="Password"
                        SortExpression="User_Pass" />
                    <asp:BoundField DataField="F_Name" HeaderText="First Name"
                        SortExpression="F_Name" />
                    <asp:BoundField DataField="L_Name" HeaderText="Last Name"
                        SortExpression="L_Name" />
                    <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />--%>

                </Columns>

                <FooterStyle BackColor="#CCCCCC" />

                <HeaderStyle BackColor="#333399" Font-Bold="True" ForeColor="Silver" />

                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />

                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />

            </cc1:SearchGridView>

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RiskQueryConnectionString %>" SelectCommand="SELECT * FROM [Bank]"></asp:SqlDataSource>

            <br />

            <br />

        </div>

        <div>
        </div>


    </form>
</body>
</html>
