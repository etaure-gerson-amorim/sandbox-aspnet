﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CScornerGVdll.MainDemo" %>

<%@ Register assembly="SamApp.WebControls.SearchGridView" namespace="SamApp.WebControls" tagprefix="SearchGridView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>
        Searchable GridView in ASP.NET using dll
    </title>

    <script src="Registration.js" type="text/javascript"></script>
    <script src="js/yourscript.js" type="text/javascript"></script>
</head>

<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        
        <a href="http://www.c-sharpcorner.com/blogs/searchable-gridview-in-asp-net-using-dll1">
            Searchable GridView in ASP.NET using dll
        </a>

        <SearchGridView:SearchGridView ID="SearchGridView1" runat="server">
        </SearchGridView:SearchGridView>
        
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/yourscript.min.js" />
            </Scripts>
        </asp:ScriptManager>
        
    </form>
</body>
</html>
