﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
 
namespace CScornerGVdll
{
    public partial class Example : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)

        {

            if (!Page.IsPostBack)

            {

                //bind();

            }

        }

        public void bind()

        {
            SearchGridView1.DataSource = null;
            SearchGridView1.DataSourceID = "";

            SearchGridView1.DataSource = GetTable();
            SearchGridView1.DataBind();
        }

        public DataView GetTable()
        {
            SqlDataSource ds = this.SqlDataSource1;//new SqlDataSource();

            //ds.ConnectionString = @"server=IT-WSPC-F10\SQL;User Id=sa;Password=inveera@123;Initial Catalog=pankaj";

            //ds.SelectCommand = "select * from User_Login";


            if (hfSearchText.Value != "")
                ds.SelectCommand += " where " + hfSearchText.Value;

            DataView dv = (DataView)ds.Select(new DataSourceSelectArguments());

            if (hfSort.Value != "")
                dv.Sort = hfSort.Value;

            return dv;

        }

        protected void SearchGridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)

        {
            SearchGridView1.PageIndex = e.NewPageIndex;
            bind();
        }

        protected void SearchGridView1_SearchGrid(string _strSearch)

        {
            hfSearchText.Value = _strSearch;
            bind();
        }



        protected void SearchGridView1_Sorting(object sender, GridViewSortEventArgs e)

        {
            if (hfSort.Value == e.SortExpression)
                hfSort.Value = e.SortExpression + " Desc";
            else
                hfSort.Value = e.SortExpression;

            bind();

        }

    }
}