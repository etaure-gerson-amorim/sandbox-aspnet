using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;

namespace SamApp.WebControls
{
	[ParseChildren(true, "SearchFilters"), ToolboxData("<{0}:SearchGridView runat=server></{0}:SearchGridView>")]
	public class SearchGridView : GridView
	{
		public delegate void SearchGridEventHandler(string _strSearch);

		private const string SHOW_EMPTY_FOOTER = "ShowEmptyFooter";

		private const string SHOW_EMPTY_HEADER = "ShowEmptyHeader";

		private const string SHOW_TOTAL_ROWS = "ShowTotalRows";

		private const string NO_OF_ROWS = "NoOfRows";

		private const string SHOW_ROWNUM = "ShowRowNum";

		private Panel _pnlSearchFooter;

		private ImageButton _btnSearch;

		private TextBox _tbSearch;

		private DropDownList _ddlFinder;

		private ListItemCollection _lstFilter;

		public event SearchGridView.SearchGridEventHandler SearchGrid
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				//this.SearchGrid = (SearchGridView.SearchGridEventHandler)Delegate.Combine(this.SearchGrid, value);
				//this.SearchGrid += (SearchGridView.SearchGridEventHandler)Delegate.Combine(this.SearchGrid, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				//this.SearchGrid = (SearchGridView.SearchGridEventHandler)Delegate.Remove(this.SearchGrid, value);
				//this.SearchGrid -= (SearchGridView.SearchGridEventHandler)Delegate.Remove(this.SearchGrid, value);
			}
		}

		[Bindable(BindableSupport.No), Category("Appearance"), DefaultValue(true)]
		public bool ShowEmptyFooter
		{
			get
			{
				if (this.ViewState["ShowEmptyFooter"] == null)
				{
					this.ViewState["ShowEmptyFooter"] = true;
				}
				return (bool)this.ViewState["ShowEmptyFooter"];
			}
			set
			{
				this.ViewState["ShowEmptyFooter"] = value;
			}
		}

		[Bindable(BindableSupport.No), Category("Appearance"), DefaultValue(true)]
		public bool ShowEmptyHeader
		{
			get
			{
				if (this.ViewState["ShowEmptyHeader"] == null)
				{
					this.ViewState["ShowEmptyHeader"] = true;
				}
				return (bool)this.ViewState["ShowEmptyHeader"];
			}
			set
			{
				this.ViewState["ShowEmptyHeader"] = value;
			}
		}

		[Bindable(BindableSupport.No), Category("Behavior"), Editor(typeof(ListItemsCollectionEditor), typeof(UITypeEditor)), MergableProperty(false), PersistenceMode(PersistenceMode.InnerProperty)]
		public virtual ListItemCollection SearchFilters
		{
			get
			{
				if (this._lstFilter == null)
				{
					this._lstFilter = new ListItemCollection();
					((IStateManager)this._lstFilter).TrackViewState();
				}
				return this._lstFilter;
			}
		}

		[Bindable(BindableSupport.No), Category("Appearance"), DefaultValue(true)]
		public bool ShowTotalRows
		{
			get
			{
				if (this.ViewState["ShowTotalRows"] == null)
				{
					this.ViewState["ShowTotalRows"] = true;
				}
				return (bool)this.ViewState["ShowTotalRows"];
			}
			set
			{
				this.ViewState["ShowTotalRows"] = value;
			}
		}

		[Bindable(BindableSupport.No), Category("Appearance"), DefaultValue(true)]
		public bool ShowRowNumber
		{
			get
			{
				if (this.ViewState["ShowRowNum"] == null)
				{
					this.ViewState["ShowRowNum"] = true;
				}
				return (bool)this.ViewState["ShowRowNum"];
			}
			set
			{
				this.ViewState["ShowRowNum"] = value;
			}
		}

		public SearchGridView()
		{
			this._pnlSearchFooter = new Panel();
			this._tbSearch = new TextBox();
			this._tbSearch.ID = "_tbSearch";
			this._btnSearch = new ImageButton();
			this._btnSearch.ID = "_btnSearch";
			this._ddlFinder = new DropDownList();
			this._ddlFinder.ID = "_ddlFinder";
			this.ShowFooter = true;
		}

		protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
		{
			int num = base.CreateChildControls(dataSource, dataBinding);
			if (num == 0 && (this.ShowEmptyFooter || this.ShowEmptyHeader))
			{
				Table table = this.CreateChildTable();
				DataControlField[] array;
				if (this.AutoGenerateColumns)
				{
					ICollection collection = this.CreateColumns(new PagedDataSource
					{
						DataSource = dataSource
					}, true);
					array = new DataControlField[collection.Count];
					collection.CopyTo(array, 0);
				}
				else
				{
					array = new DataControlField[this.Columns.Count];
					this.Columns.CopyTo(array, 0);
				}
				if (this.ShowEmptyHeader)
				{
					GridViewRow row = base.CreateRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
					this.InitializeRow(row, array);
					this.OnRowCreated(new GridViewRowEventArgs(row));
					table.Rows.Add(row);
				}
				GridViewRow gridViewRow = new GridViewRow(-1, -1, DataControlRowType.EmptyDataRow, DataControlRowState.Normal);
				TableCell tableCell = new TableCell();
				tableCell.ColumnSpan = array.Length;
				tableCell.Width = Unit.Percentage(100.0);
				if (this.EmptyDataTemplate != null)
				{
					this.EmptyDataTemplate.InstantiateIn(tableCell);
				}
				else if (!string.IsNullOrEmpty(this.EmptyDataText))
				{
					tableCell.Controls.Add(new LiteralControl(this.EmptyDataText));
				}
				gridViewRow.Cells.Add(tableCell);
				table.Rows.Add(gridViewRow);
				if (this.ShowEmptyFooter)
				{
					GridViewRow row2 = base.CreateRow(-1, -1, DataControlRowType.Footer, DataControlRowState.Normal);
					this.InitializeRow(row2, array);
					this.OnRowCreated(new GridViewRowEventArgs(row2));
					table.Rows.Add(row2);
				}
				this.Controls.Clear();
				this.Controls.Add(table);
			}
			return num;
		}

		protected override ICollection CreateColumns(PagedDataSource dataSource, bool useDataSource)
		{
			if (dataSource != null)
			{
				this.ViewState["NoOfRows"] = dataSource.DataSourceCount;
			}
			return base.CreateColumns(dataSource, useDataSource);
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			if (!this.IsDesign() && this.ShowRowNumber)
			{
				TemplateField templateField = new TemplateField();
				NumberColumn itemTemplate = new NumberColumn();
				templateField.ItemTemplate = itemTemplate;
				this.Columns.Insert(0, templateField);
			}
		}

		protected override void OnRowCreated(GridViewRowEventArgs e)
		{
			base.OnRowCreated(e);
			if (!this.IsDesign())
			{
				if (e.Row.RowType == DataControlRowType.Footer && this.ShowFooter && e.Row.Cells.Count > 0)
				{
					if (this.ShowTotalRows)
					{
						e.Row.Cells[0].Text = this.ViewState["NoOfRows"] + " Rows.";
					}
					if (e.Row.Cells[e.Row.Cells.Count - 1].Controls.Count == 0)
					{
						Table table = new Table();
						table.Style.Add("width", "100%");
						table.Style.Add("align", "right");
						TableRow tableRow = new TableRow();
						TableCell tableCell = new TableCell();
						tableCell.Style.Add("align", "right");
						tableCell.Style.Add("width", "100%");
						if (this._ddlFinder.Items.Count == 0)
						{
							this.SetFilter();
						}
						this._btnSearch.Width = 20;
						this._btnSearch.Height = 20;
						this._btnSearch.ImageAlign = ImageAlign.AbsMiddle;
						this._btnSearch.AlternateText = "Search";
						this._btnSearch.Click += new ImageClickEventHandler(this._btnSearch_Click);
						tableCell.Controls.Add(this._ddlFinder);
						tableCell.Controls.Add(this._tbSearch);
						tableCell.Controls.Add(this._btnSearch);
						tableRow.Cells.Add(tableCell);
						table.Rows.Add(tableRow);
						this._pnlSearchFooter.Controls.Add(table);
						e.Row.Cells[e.Row.Cells.Count - 1].Controls.Add(this._pnlSearchFooter);
					}
				}
				if (e.Row.RowType == DataControlRowType.Header)
				{
					if (this.ShowRowNumber && this.ShowHeader)
					{
						e.Row.Cells[0].Text = "Sno";
						return;
					}
				}
				else if (e.Row.RowType == DataControlRowType.DataRow && this.ShowRowNumber)
				{
					e.Row.Cells[0].Text = (e.Row.RowIndex + this.PageSize * this.PageIndex + 1).ToString();
				}
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (this._btnSearch.ImageUrl == null || this._btnSearch.ImageUrl == "")
			{
				this._btnSearch.ImageUrl = this.Page.ClientScript.GetWebResourceUrl(base.GetType(), "SamApp.WebControls.search.bmp");
			}
		}

		private bool IsDesign()
		{
			return base.Site != null && base.Site.DesignMode;
		}

		public void SetFilter()
		{
			this._ddlFinder.Items.Clear();
			foreach (ListItem item in this.SearchFilters)
			{
				this._ddlFinder.Items.Add(item);
			}
		}

		protected string ConstructSearchString()
		{
			string text = this._tbSearch.Text.Trim();
			if (text == string.Empty)
			{
				return string.Empty;
			}
			return this._ddlFinder.SelectedValue + " like '" + text + "%'";
		}

		private void _btnSearch_Click(object sender, ImageClickEventArgs e)
		{
			string strSearch = this.ConstructSearchString();
			this.OnSearchGrid(strSearch);
		}

		protected void OnSearchGrid(string _strSearch)
		{
			//if (this.SearchGrid != null)
			//{
			//	this.SearchGrid(_strSearch);
			//}
		}
	}
}
