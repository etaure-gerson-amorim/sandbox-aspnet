using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyCompany("SamApp")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © SamApp 2009")]
[assembly: AssemblyDescription("Searchable Grid View showing number of rows, serial number and fixed header and footer.")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("SearchGridView")]
[assembly: AssemblyTitle("SearchGridView")]
[assembly: AssemblyTrademark("")]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: TagPrefix("SamApp.WebControls", "SearchGridView")]
[assembly: WebResource("SamApp.WebControls.search.bmp", "image/bmp")]
